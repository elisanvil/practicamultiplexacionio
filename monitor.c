#include <libudev.h>
#include "csapp.h"

static struct udev_device *obtener_hijo(struct udev*, struct udev_device*, const char*);
static void check_device(struct udev*, struct udev_device*);

int main(int argc, char **argv)
{
	struct udev *udev;
	struct udev_monitor *mon;
	struct udev_device *dev;
	int listenfd, connfd, monitor_usb_fd;
	struct sockaddr_in clientaddr;
	char *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	/* Create the udev object */
	udev = udev_new();
	if (!udev) {
		printf("Can't create udev\n");
		exit(1);
	}

	/* Crea un monitor de dispositivos usb */
	mon = udev_monitor_new_from_netlink(udev, "udev");
	udev_monitor_filter_add_match_subsystem_devtype(mon, "scsi", "scsi_device");
	udev_monitor_enable_receiving(mon);

	/* Retorna un descriptor de archivos que luego será utilizado con select */
	monitor_usb_fd = udev_monitor_get_fd(mon);

	listenfd = Open_listenfd(port);
	unsigned int clientlen = sizeof(clientaddr);
	

	while(1)
	{

        printf("Ingrese un comando: ");
        fgets(command,256,stdin);
        
        send((int)clientfd, command, sizeof(command), 0);        
        
		fd_set fds;
		struct timeval tv;

		FD_ZERO(&fds);
		FD_SET(monitor_usb_fd, &fds);
		FD_SET(listenfd, &fds);
		FD_SET(STDIN_FILENO, &fds);
		tv.tv_sec = 0;
		tv.tv_usec = 0;

		Select(listenfd+1, &fds, NULL, NULL, &tv);
		if (FD_ISSET(monitor_usb_fd, &fds))
		{
			dev = udev_monitor_receive_device(mon);
			check_device(udev, dev);
		}
	}


	udev_unref(udev);

	return 0;

}

static void check_device(struct udev *udev, struct udev_device* dev)
{
			
	if (dev) {
		
		struct udev_device* usb = udev_device_get_parent_with_subsystem_devtype(dev,"usb","usb_device");

		if(usb)
		{
			sleep(2);
			struct udev_device* block = obtener_hijo(udev, dev, "block");
			if(block) {
				printf("Got Mass Storage USB Device\n");
				printf("   Node: %s\n", udev_device_get_devnode(block));
				printf("   Subsystem: %s\n", udev_device_get_subsystem(block));
				printf("   Devtype: %s\n", udev_device_get_devtype(block));
				printf("   Action: %s\n", udev_device_get_action(dev));
				printf("   USB ID: %s:%s\n", udev_device_get_sysattr_value(usb, "idVendor"),
											udev_device_get_sysattr_value(usb, "idProduct"));
				udev_device_unref(block);
			}else {
				printf("   USB ID: %s:%s\n", udev_device_get_sysattr_value(usb, "idVendor"),
											udev_device_get_sysattr_value(usb, "idProduct"));
				printf("   Action: %s\n", udev_device_get_action(dev));
			}
			udev_device_unref(usb);
		}
		
		udev_device_unref(dev);
	}
	else {
		printf("No Device from receive_device(). An error occured.\n");
	}
}

static struct udev_device *obtener_hijo(struct udev* udev, struct udev_device* padre, const char* subsistema)
{
	struct udev_device* hijo = NULL;
	struct udev_enumerate *enumerar = udev_enumerate_new(udev);

	udev_enumerate_add_match_parent(enumerar, padre);
	udev_enumerate_add_match_subsystem(enumerar, subsistema);
	udev_enumerate_scan_devices(enumerar);

	struct udev_list_entry *dispositivos = udev_enumerate_get_list_entry(enumerar);
	struct udev_list_entry *entrada;

	udev_list_entry_foreach(entrada, dispositivos) {
		const char *ruta = udev_list_entry_get_name(entrada);
		hijo = udev_device_new_from_syspath(udev, ruta);
		break;
	}

	udev_enumerate_unref(enumerar);
	return hijo;
}
